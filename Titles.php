<?php declare(strict_types=1);

namespace projekt;

require __DIR__ . '/vendor/autoload.php';

use League\Csv\Reader;

$csv = Reader::createFromPath('csv/sales-orders-lines.csv','r');
$csv->setDelimiter(';');
$csv->setHeaderOffset(0);

$records = $csv->getRecords();
$header = $csv->getHeader();
?>
<html>
    <head>
        <style>
        h1{
            color: blue;
        }
        a{
            color: lightblue;
            text-decoration: none;
        }

        a:hover{
            text-decoration: underline;
        }
        </style>
    </head>

    <body>
        <h1>Titles and records</h1>
        <nav>
            <ul>
                <li>
                    <a href="/">Back</a>
                </li>
            </ul>
        </nav>
    </body>
</html>
<?php
foreach  ($header as $header)
{
    echo $header . "<br/>";

}
foreach($records as $offset => $record){
    foreach($record as $record){
        echo $record;
    }
    echo "<br/>";
}
?>
